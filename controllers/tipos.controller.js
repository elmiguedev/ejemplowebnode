// obtiene el dal
const dalTipoCosa = require("../dal/table.dal")("TipoCosa");

// crea el controller
const controller = {};

// le define las acciones principales
controller.indexView = async function (req, res) {
    // obtiene los tipos de cosa
    const tipos = await dalTipoCosa.obtenerTodo();
    // muestra la vista
    res.render("tipos/index", {
        tipos: tipos
    });
}

controller.registrar = async function (req, res) {

    /* 
        pregunta si viene con id, si viene con id
        entonces es una edicion, sino es algo nuevo
    */

    if (req.body.id) {

        // si tiene id, edita
        await dalTipoCosa.actualizar(req.body);

    } else {

        // si no tiene id, inserta
        await dalTipoCosa.insertar({
            nombre: req.body.nombre
        });

    }

    // luego, retorna la misma vista
    res.redirect("/tipos");

}

controller.editarView = async function (req, res) {

    // obtiene el item que se quiere editar
    const tipo = await dalTipoCosa.obtenerUno(req.params.id);

    // obtiene los tipos de cosa
    const tipos = await dalTipoCosa.obtenerTodo();

    // muestra la vista
    res.render("tipos/index", {
        tipos: tipos,
        tipo: tipo
    });
}

controller.eliminar = async function (req, res) {
    // elimina el objeto
    await dalTipoCosa.eliminar(req.params.id);

    // redirecciona a la vista del listado
    res.redirect("/tipos");
}



// exporta el controller
module.exports = controller;