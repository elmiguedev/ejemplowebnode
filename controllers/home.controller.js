// crea el controller
const controller = {};

// le define las acciones principales
controller.index = function (req, res) {
    res.render("inicio");
}

// exporta el controller
module.exports = controller;