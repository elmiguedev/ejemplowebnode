const path = require("path");
const databaseUrl = path.resolve(__dirname, "../db/db.sqlite");
const database = require('sqlite3-wrapper').open(databaseUrl);

const db = {};

db.query = (query) => {
    return new Promise((resolve, reject) => {
        database.select(query, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    });
}

db.select = (table, where) => {
    return new Promise((resolve, reject) => {
        database.select({
            table: table,
            where: where
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    });
}

db.findOne = (table, where) => {
    return new Promise((resolve, reject) => {
        database.select({
            table: table,
            where: where
        }, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data[0]);
            }
        })
    });
}

db.insert = (table, record) => {
    return new Promise((resolve, reject) => {
        database.insert(table, record, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}


db.update = (table, where, record) => {
    return new Promise((resolve, reject) => {
        database.update(table, where, record, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

db.delete = (table, where) => {
    return new Promise((resolve, reject) => {
        database.delete(table, where, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}


module.exports = db;