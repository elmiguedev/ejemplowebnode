const db = require("./base.dal");
const tabledal = (tabla) => {
    const dal = {};

    dal.obtenerTodo = function () {
        return db.select(tabla);
    }

    dal.obtenerUno = function (id) {
        return db.findOne(tabla, { id: id });
    }

    dal.insertar = function (cosa) {
        // devuelve la PK de la cosa que se crea
        return db.insert(tabla, cosa);
    }

    dal.actualizar = function (cosa) {
        return db.update(tabla, { id: cosa.id }, cosa);
    }

    dal.eliminar = function (id) {
        return db.delete(tabla, { id: id });
    }
    return dal;
};

module.exports = tabledal;
